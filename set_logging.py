import datetime,os
import argparse
import logging
import sys

#https://realpython.com/python-logging/


class TimeFilter(logging.Filter):
#https://stackoverflow.com/questions/31521859/python-logging-module-time-since-last-log

    def filter(self, record):
        try:
          last = self.last
        except AttributeError:
          last = record.relativeCreated

        delta = datetime.datetime.fromtimestamp(record.relativeCreated/1000.0) - datetime.datetime.fromtimestamp(last/1000.0)

        record.relative = '{0:.3f}'.format(delta.seconds + delta.microseconds/1000000.0)

        self.last = record.relativeCreated
        return True

class InfoFilter(logging.Filter):
    #https://stackoverflow.com/questions/16061641/python-logging-split-between-stdout-and-stderr
    def filter(self, record):
        return record.levelno in (logging.DEBUG, logging.INFO)

def main(basename=None,verbose=False):
    root = logging.getLogger()
    root.setLevel(logging.DEBUG)

    # Set file handler
    if basename is not None:
        fh_info = logging.FileHandler(os.path.join(os.getcwd(), basename+'.log'), 'w+')
    else:
        fh_info = logging.FileHandler(os.path.join(os.getcwd(), 'infolog'+datetime.datetime.now().strftime("_%d-%m-%Y_%H:%M:%S")+'.log'), 'w+')
    fh_info.setLevel(logging.INFO)
    fh_info.addFilter(TimeFilter())

    formatter = logging.Formatter(
        '%(message)-100s - %(asctime)s (%(relative)s) - %(process)8s - %(name)20s - %(levelname)8s')
    fh_info.setFormatter(formatter)

    root.addHandler(fh_info)

    # Set stream (console) handler
    sh_formatter = logging.Formatter('%(asctime)s - %(message)s')

    # Write log msgs of level WARNING or greater to stderr
    sh_err = logging.StreamHandler()
    sh_err.setLevel(logging.WARNING)
    sh_err.setFormatter(sh_formatter)

    root.addHandler(sh_err)

    if verbose:
        # Write log msgs of level INFO or DEBUG to stdout
        sh_out = logging.StreamHandler(sys.stdout)
        sh_out.setLevel(logging.INFO)
        sh_out.addFilter(InfoFilter())
        sh_out.setFormatter(sh_formatter)

        root.addHandler(sh_out)

    root.info('Logging initialised') # Will be written into the info.log, debug.log and the stream


if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='set_logging.py', formatter_class=argparse.ArgumentDefaultsHelpFormatter, \
                                     description="Initialise the root logger.")

    parser.add_argument("-b", "--basename", type=str, default=None,
                            help="Base name of logging file.")
    parser.add_argument("-v", "--verbose", action='store_true', default=False, \
                            help="Log info messages to standard output.")

    args = parser.parse_args()
    main(args.basename,args.verbose)

