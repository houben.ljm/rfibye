#!/usr/bin/env python

"""
RFIbye.py

Reads in a FIL and PRESTO mask file to replace RFI contaminated
data samples with average values to create a MASKED FIL file.
Output is a MASKED filterbank file.

Leon Houben
"""

import os,sys
import datetime
import argparse
import inspectra
import fil_looper
import mask_maker
import set_logging
import logging
import numpy as np
import scipy as sp
import bottleneck as bn
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from scipy.interpolate import interp1d

logger = logging.getLogger(__name__)


class RFI_mitigator(object):
    """
    Collection of handy RFI removal tools.
    """
    def __init__(self,norm_data=False,ts_thres=False,ts_downsampfrac=False,std_thres=False, \
                      chan_thres=False,chan_int_extr_thres=2.5,edge=0,rm_reg=False, \
                      dtype='uint8',zap_chans=None,rfifind_mask=None, \
                      rm_band_edge=False,rm_bandpass=False,chan_indep=False, \
                      zap_chan_thres=False,full_spec_flag=30., full_chan_zap=20., \
                      zerodm=False, write_mask=False,inspec=False,verbose=False):
        """RFI mitigator constructor

            Input:
                ... :

            Output:
                RFI_mitigator object
        """
        self.norm_data   = norm_data
        self.ts_thres    = ts_thres
        self.ts_downfrac = ts_downsampfrac
        self.std_thres   = std_thres
        self.chan_thres  = chan_thres
        self.int_extr    = chan_int_extr_thres
        self.edge        = edge
        self.rm_reg      = rm_reg
        self.dtype       = dtype
        if dtype.startswith('uint'):
            self.idtype  = np.iinfo(dtype)
        elif dtype.startswith('float'):
            self.idtype  = np.finfo(dtype)
        else:
            log_str = "\nThe data type should be either 'uint' or 'float', not %s..." % dtype
            logger.error(log_str)
            raise TypeError(log_str)
        if type(zap_chans) == str:
            ## zap_chans must be parsed to a list first
            # Assuming a comma seperated list of channel numbers
            zap_chans = zap_chans.split(",")
            zap_list = []
            for num in zap_chans:
                try:
                    zap_list.append(int(num))
                except ValueError:
                    logger.exception("\nParsing channel list, interpretting %s as channel range instead of a single channel number." % num)
                    range_colon = num.split(":")
                    range_dash  = num.split("-")

                    if len(range_colon) == 2:
                        zap_list.extend(list(np.arange(int(range_colon[0]),int(range_colon[1])+1)))
                    elif len(range_dash) == 2:
                        zap_list.extend(list(np.arange(int(range_dash[0]),int(range_dash[1])+1)))
                    else:
                        log_str = "\nCan not recognise %s as a channel number range to flag..." % num
                        logger.error(log_str)
                        raise ValueError(log_str)

            self.zap_chans = np.array(zap_list)
        elif zap_chans is not None:
            self.zap_chans = np.array(zap_chans)
        else:
            self.zap_chans = zap_chans
        self.rm_band_edge  = rm_band_edge
        self.rm_bandpass   = rm_bandpass
        self.chan_indep    = chan_indep
        self.zap_chan_thres= zap_chan_thres
        self.full_spec_flag= full_spec_flag
        self.full_chan_zap = full_chan_zap
        self.zerodm        = zerodm
        self.write_mask    = write_mask
        if write_mask and rfifind_mask is not None:
            logger.warning("\n\nWARNING: the RFIbye mask may become very large due to the inclusion of an rfifind mask!\n")
        self.inspec      = inspec
        self.verbose     = verbose

        ## Define counters for the ammount of flagged data
        self.N_flagged_spec = 0
        self.N_flagged_chan = 0
        self.N_flagged_samp = 0
        self.N_flagged_samples = 0 # Total flagged sample count
        if rfifind_mask is not None:
            self.rfifind = mask_maker.masker(rfifind_mask)


    #######################################
    # Define useful class methods 
    #######################################
    def bn_median(self, masked_array, axis=None):
        """
        Perform fast median on masked array
        """
        data = masked_array.filled(fill_value=np.NaN)
        med = bn.nanmedian(data, axis=axis)
        # construct a masked array result, setting the mask from any NaN entries
        return np.ma.array(med, mask=np.isnan(med))

    def bn_std(self, masked_array, axis=None):
        """
        Perform fast median on masked array
        """
        data = masked_array.filled(fill_value=np.NaN)
        std = bn.nanstd(data, axis=axis)
        # construct a masked array result, setting the mask from any NaN entries
        return np.ma.array(std, mask=np.isnan(std))

    def nfspec(self,masked_data):
        # Calc number of fully flagged spectra
        return np.sum(masked_data.mask.all(axis=0))

    def nfchan(self,masked_data):
        # Calc number of fully flagged channels
        return np.sum(masked_data.mask.all(axis=1))

    def nfsamp(self,masked_data):
        # Calc number of flagged samples
        return np.sum(masked_data.mask)

    #################################
    # Main RFI removal function
    #################################

    def clean(self,data,Nblock,**kwargs):
        """Replace RFI with random values.

            Input:
                data:   A 2D numpy array of shape (nchan,nspec).
                Nblock: Block number that is being cleaned.
                kwargs: All additional namespaced arguments from fil_looper.

            Output:
                A cleaned copy of data.
        """

        ## set flag counters to zero
        self.N_flagged_spec = 0
        self.N_flagged_chan = 0
        self.N_flagged_samp = 0

        ## Make numpy masked array
        masked_data = np.ma.array(data, mask=False, fill_value=0)

        ## Normalise data
        if self.norm_data and not masked_data.mask.any():
            logger.info("    Removing median and standard deviation...")
            self.med0 = self.bn_median(masked_data, axis=1) # Use bottleneck median here since its faster
            self.std0 = np.std(masked_data.data, axis=1)
            masked_data = (masked_data-self.med0[:,None])/self.std0[:,None]
            logger.info("        DONE!")

        ## Flag RFI in freq
        if (self.rm_band_edge is not False) and not masked_data.mask.all():
            # Remove the bandpass edges
            logger.info("    Removing the bandpass edges...")
            masked_data = self.remove_band_edges(masked_data, self.rm_band_edge)
        if (self.zap_chans is not None) and not masked_data.mask.all():
            # Zap bad channels
            logger.info("    Zapping bad channels...")
            masked_data = self.zap_channels(masked_data, self.zap_chans, Nblock)
        if hasattr(self,'rfifind') and not masked_data.mask.all():
            # Zap channels from rfifind mask
            logger.info("    Zapping channels according to the given rfifind mask...")
            masked_data = self.apply_rfifind_mask(masked_data, Nblock)
        if (self.zap_chan_thres is not False) and not masked_data.mask.all():
            # Threshold the channels
            logger.info("    Thresholding all channels...")
            masked_data = self.zap_overpowered(masked_data,self.zap_chan_thres)

        ## Flag RFI in time
        if (self.rm_reg is not False) and not masked_data.mask.all():
            # Remove regular
            logger.info("    Removing periodic RFI...")
            masked_data = self.TS_remove_regular(masked_data,kwargs['nspec'],self.rm_reg)
        if (self.ts_thres is not False) and not masked_data.mask.all():
            # Threshold time series
            logger.info("    Thresholding the time series...")
            masked_data = self.TS_threshold(masked_data,self.ts_thres)
        if (self.ts_downfrac is not False) and not masked_data.mask.all():
            # Threshold downsampled time series
            logger.info(f"    Thresholding the time series downsampled with a factor {2**self.ts_downfrac}...")
            masked_data = self.TS_threshold_downsampled(masked_data,self.ts_thres,self.ts_downfrac)
        if (self.std_thres is not False) and not masked_data.mask.all():
            # Threshold standard deviation of data block
            logger.info("    Thresholding the standard deviation of the loaded data BLOCK...")
            masked_data = self.std_threshold(masked_data,self.std_thres)
        if (self.chan_thres is not False) and not masked_data.mask.all():
            # Threshold channels
            logger.info("    Thresholding the individual channels...")
            masked_data = self.chan_threshold(masked_data,self.chan_thres,self.int_extr)


        if not masked_data.mask.all():
            ## Percentage of fully masked channels
            fchanper = self.N_flagged_chan/masked_data.shape[0]

            ## Set spectrum flag threshold incorporating the ammount of fully zapped channels
            if (fchanper > self.full_spec_flag/100.):
                fsf_percentage = fchanper + (1.-fchanper)*(self.full_spec_flag/100.)
                fsf_percentage *= 100.
            else:
                fsf_percentage = self.full_spec_flag

            ## Smooth the time flagging, i.e. throw away more full spectra is close to or is heavily contaminated with RFI
            logger.info(f"    Flagging spectra with more than {fsf_percentage}% of its channels zapped...")
            masked_data = self.smooth_time_flagging(masked_data,fsf_percentage)

            ## Percentage of fully masked spectra
            fspecper = self.N_flagged_spec/masked_data.shape[1]

            ## Set channel zap thresholds incorporating the ammount of fully flagged spectra
            if (fspecper > self.full_chan_zap/100.):
                fcz_percentage = fspecper + (1.-fspecper)*(self.full_chan_zap/100.)
                fcz_percentage *= 100.
            else:
                fcz_percentage = self.full_chan_zap

            ## Zap channels that have more spectra flagged than given percentage
            logger.info(f"    Zapping channels with more than {fcz_percentage}% of its spectra flagged...")
            masked_data = self.smooth_freq_zapping(masked_data,fcz_percentage)


        if self.norm_data:
            ## If opperations were performed on normalised data, re-add the med and std
            logger.info("    Re-adding median and standard deviation...")
            masked_data = np.ma.array(masked_data.data*self.std0[:,None] + self.med0[:,None], mask=masked_data.mask)
            logger.info("        DONE!")


        ## Show mask
        if self.inspec:
            inspec_plot = inspectra.inspectra(masked_data, kwargs['nspec'], tsamp=kwargs['tsamp'], fch1=kwargs['fch1'], \
                                              foff=kwargs['foff'], title='Check the appropriatness of the determined mask.')
            self.inspec = inspec_plot()[0]
            del inspec_plot # Free memory

        ## Write mask to file
        if self.write_mask:
            logger.info("    Writting mask to file...")
            if Nblock == 0:
                # Create mask file 
                if (self.zap_chans is None) and not hasattr(self,'rfifind'):
                    self.write_mask.make_rfibye_mask(np.array([]))
                elif self.zap_chans is None:
                    self.write_mask.make_rfibye_mask(self.rfifind.zap_chans)
                elif not hasattr(self,'rfifind'):
                    self.write_mask.make_rfibye_mask(self.zap_chans)
                else:
                    self.write_mask.make_rfibye_mask(np.union1d(self.zap_chans,self.rfifind.zap_chans))

            # Write mask to file
            self.write_mask.write_to_rfibye_mask(np.copy(masked_data.mask))

        ## Apply mask
        logger.info("    Applying mask to data...")
        data = self.apply_mask(masked_data,Nblock)

        ## Zero-DM filtering
        if self.zerodm:
            logger.info("    Applying zero DM filter...")
            data = self.zero_DMing(data)

        ## Remove bandpass
        if self.rm_bandpass:
            logger.info("    Removing bandpass from data block...")
            data = self.remove_bandpass(data,self.chan_indep)

        ## Round values in case of integer data
        if self.dtype.startswith('uint'):
            logger.info("    Rounding all values from data block...")
            data = np.around(data)

        return data

    #################################
    # General operation functions
    #################################

    def apply_mask(self,masked_data,Nblock):
        """Determine the noise distribution per channel and replace
           flagged data elements with random noise having the same distribution
           as the channel noise.
           If a complete channel is flagged it will be replaced with random
           Gaussian noise.
           If the entire BLOCK is flagged it will be replaced with random
           noise having the same distribution as the previous data BLOCK. Though,
           in the case of multiprocessing (where no data is available from a
           previous run) the code will terminate.

            Input:
                masked_data: A 2D numpy masked array of shape (nchan,nspec).
                Nblock: The block number being processed.

            Output:
                The data block with its mask hard applied to it
        """
        ## Register the number of flagged samples
        self.N_flagged_samples += self.nfsamp(masked_data)

        ## Make random noise
        if masked_data.mask.all() and (Nblock == 0):
            log_str = "\nAll elements of first data BLOCK are being flagged, \
                              check your data, remove the first %i spectra and try again." % masked_data.shape[1]
            logger.error(log_str)
            raise ValueError(log_str)
        elif masked_data.mask.all() and not hasattr(self,'noise'):
            log_str = f"\n All elements of BLOCK {Nblock} are being flagged \
                        and no previous bandpass statistics are available.\n        Use Single Core Processing for this data file!"
            logger.error(log_str)
            raise ValueError(log_str)                        
        elif masked_data.mask.all():
            logger.warning("\n\nWARNING: All elements of BLOCK %i are being flagged!\n \
        The bandpass statistics of the previous BLOCK will be used to create a fully fake BLOCK of data.\n" % Nblock)
            
            # Use previous noise array to determine BLOCK statistics
            data_stats = np.ma.array(self.noise, mask=False)
        else:
            # Use current data to determine BLOCK statistics
            data_stats = masked_data

        # Get size data BLOCK
        nchan,nspec = masked_data.shape

        # Create a noise array by shuffeling all valid elements of each channel
        indices = np.repeat([np.arange(0,nspec)],nchan,axis=0)
        indices += np.arange(0,nspec*nchan,nspec)[:,None]
        valid_mask = ~data_stats.mask.flatten()

        num_vals = np.sum(~data_stats.mask[~data_stats.mask.all(1)],axis=1)
        val_count = num_vals.min()
        num_vals -= val_count
        num_vals = np.repeat(num_vals,2)
        num_vals[::2] = val_count

        bool_arr = np.tile(np.array([True,False]),int(len(num_vals)/2))
        bool_arr = np.repeat(bool_arr,num_vals)

        valid_vals = indices.flatten()[valid_mask][bool_arr]

        full_valid_arr = np.zeros((nchan,val_count), dtype=int)
        full_valid_arr[~data_stats.mask.all(1)] = valid_vals.reshape(-1,val_count)

        rand_ints = np.random.choice(np.arange(0,val_count),(nchan,nspec))
        rand_ints += np.arange(0,val_count*nchan,val_count)[:,None]
        noise_ints = np.take(full_valid_arr, rand_ints)
        self.noise = np.take(data_stats.data, noise_ints)
        med_bandpass = np.median(self.noise,axis=1)
        std_bandpass = np.std(self.noise,axis=1)

        # Add bandpass mask
        bandpass_mask = data_stats.mask.all(axis=1)
        med_bandpass = np.ma.array(med_bandpass, mask=bandpass_mask)
        std_bandpass = np.ma.array(std_bandpass, mask=bandpass_mask)
        
        # Replace flagged channels with linear interpolation of nearest valid values
        if np.any(bandpass_mask):
            med_bandpass_filled = self.fill_outliers(med_bandpass)
            std_bandpass_filled = self.fill_outliers(std_bandpass)

            zap_list = np.argwhere(bandpass_mask).flatten()
            block_zaps = np.setdiff1d(zap_list, self.zap_chans)
            if block_zaps.size > 0:
                data_med = np.median(data_stats.data[block_zaps], axis=1)
                data_std = np.std(data_stats.data[block_zaps], axis=1)

                med_bandpass_filled[block_zaps] = data_med
                std_bandpass_filled[block_zaps] = data_std


            # Substitute flagged channels with Gaussian noise
            self.noise[bandpass_mask] = np.random.normal(med_bandpass_filled[bandpass_mask],abs(std_bandpass_filled[bandpass_mask]),(nspec,np.sum(bandpass_mask))).T

        ## Merge masked data with noise
        logger.info("        Mask applied!")
        return np.where(masked_data.mask, self.noise, masked_data.data)


    def fill_outliers(self,oneD_maskedarray,kind='linear'):
        """Detect outliers and replace them with interpolated values.
           Outliers are defined as those values bigger than the given threshold.
        """
        logger.debug("   --- Replace outliers with nearest or interpolated values")
        x = np.arange(len(oneD_maskedarray))
        valid = ~oneD_maskedarray.mask
        interpol = interp1d(x[valid], oneD_maskedarray[valid], kind=kind)

        # Fill outliers with interpolation of nearest valid values
        x_fill = np.arange(x[valid][0],x[valid][-1],1)
        filled = interpol(x_fill)

        # If band edges were removed, interchange these values with nearest valid value
        if x_fill[0] > x[0]:
            filled_st = np.ones(x_fill[0])*filled[0]
            filled = np.append(filled_st,filled)
        if x_fill[-1] < x[-1]:
            filled_lst = np.ones(x[-1] - x_fill[-1])*filled[-1]
            filled = np.append(filled, filled_lst)

        return filled


    def add_edges(self,index_array,N0,N1,ax,edge=None):
        """Add edge number of elements to either side of the elements in index_array.

            Input:
                index_array: 2D array with index elements that need additional elements to be flagged.
                N0:          Total number of elements 0-th axis of the array from which the index array is given.
                N1:          Total number of elements 1-st axis of the array from which the index array is given.
                ax:          To which axis edges should be added.
                edge:        Number of elements to add to either side of the elements in index_array.

            Output:
                An extended index_array with elements to be flagged.
        """
        if edge is None:
            edge = self.edge

        # Check if index array is of correct size
        if index_array.shape[1] == 0:
            return index_array
        elif (len(index_array.shape) != 2) and (index_array.shape[0] != 2):
            log_str = "\nThe index array is of the wrong size!"
            logger.error(log_str)
            raise ValueError(log_str)

        Nflags = index_array.shape[1]
        to_flag_ext = np.tile(index_array.copy(), (1,edge*2+1))
        edges = np.repeat(np.arange(-1*edge,edge+1),Nflags)
        if ax == 0:
            to_flag_ext[0] = to_flag_ext[0]+edges
            # Clip out of bounds indices
            np.clip(to_flag_ext[0],0,int(N0)-1,out=to_flag_ext[0])
        elif ax == 1:
            to_flag_ext[1] = to_flag_ext[1]+edges
            # Clip out of bounds indices
            np.clip(to_flag_ext[1],0,int(N1)-1,out=to_flag_ext[1])
        else:
            log_str = "\nEdges should be applied to either the first (0) or second (1) axis of data!"
            logger.error(log_str)
            raise ValueError(log_str)

        logger.debug("        An additional %i edges were flagged!" % edge)
        return to_flag_ext


    #################################
    # Frequeny domain functions
    #################################

    def get_bandpass(self,data):
        ## Calculate band of cleaned data
        med_bandpass = np.median(data,axis=1)
        std_bandpass = np.std(data,axis=1)
        std_data = np.std(data)

        ## Fit 2D polynomial to get data's bandpass
        #x = np.arange(len(self.med_bandpass))
        #p = np.poly1d(np.polyfit(x,self.med_bandpass,2))
        #self.bandpass = p(x)
        #logger.info("        Bandpass determined as a 2D polynomial!")

        return med_bandpass,std_bandpass,std_data

    def remove_bandpass(self,data,chan_indep=False):
        """
        Remove bandpass from data and scale to fill dtype range (if integer data).
        NOTE: any mask should be applied before removing the bandpass!
        """
        ## Scale data
        # Get median lines
        fit = np.polynomial.polynomial.polyfit(np.arange(data.shape[1]), data.T, 1)
        med_lines = np.polynomial.polynomial.polyval(np.arange(data.shape[1]), fit)

        if chan_indep:
            logger.info("        Scaling data using the std of each channel.")
            scaled_data = (data - med_lines) / np.std(data, axis=1)[:,None]
        else:
            logger.info("        Scaling data using the std of the entire data BLOCK.")
            scaled_data = (data - med_lines) / np.std(data)

        if self.dtype.startswith('uint'):
            # Get dtype and cleaned data stats
            dtype_med = (self.idtype.max - self.idtype.min) / 2
            dtype_std = (self.idtype.max - dtype_med) / 5 #NOTE: hard code a dtype std so one doens't get jumps in data blocks

            ## Print progress
            logger.info("        Bandpass removed; data has now a median of %i and was scaled with a factor %f." \
                        % (dtype_med,dtype_std))
            # Return data with median 'dtype median' and std to fill dtype range
            return scaled_data*dtype_std + dtype_med

        else:
            ## Print progress
            logger.info("        Bandpass removed; data has now a median of '0' and std of '1'.")
            # Return data with median '0' and std '1'
            return scaled_data

    def remove_band_edges(self,masked_data,percent):
        """
        Flag the top and bottom percent%% of the bandpass.
        """
        if percent is None:
            percent = 1. # Default value

        # Channels to flag
        rm_chans      = np.ceil(masked_data.shape[0]*(percent/100.)).astype(int)
        chans         = np.arange(masked_data.shape[0])
        zap_band_edge = np.append(chans[:rm_chans],chans[-rm_chans:])

        # Update zap channel list
        if self.zap_chans is None:
            self.zap_chans = zap_band_edge
        else:
            self.zap_chans = np.union1d(self.zap_chans,zap_band_edge)
        self.rm_band_edge = False # Band edges will be flagged with zap_channels()

        ## Print progress
        logger.info("        The top and bottom %0.2f%% of band will be flagged." % percent)
        logger.info("        Edges will be removed by the zap_channels function!")

        return masked_data

    def zap_channels(self,masked_data,zap_list,Nblock):
        # Check if manual zap list is contained in rfifind mask
        if (Nblock == 0) and hasattr(self,'rfifind'):
            if len(np.setdiff1d(zap_list,self.rfifind.zap_chans)) == 0:
                self.zap_chans = None
                ## Print progress
                logger.warning("        Given channels to manually zap are contained within the specified rfifind mask!")
                return masked_data
            else:
                self.zap_chans = zap_list = np.setdiff1d(zap_list,self.rfifind.zap_chans)
        
        # Flag channels
        masked_data[zap_list] = np.ma.masked

        ## Print progress
        flag_chan_count = self.nfchan(masked_data) - self.N_flagged_chan
        logger.info("        DONE: %i bad channels were zapped!" % flag_chan_count)
        self.N_flagged_chan += flag_chan_count
        self.N_flagged_samp += flag_chan_count * masked_data.shape[1]

        return masked_data

    def apply_rfifind_mask(self,masked_data,Nblock):
        # Flag channels of the Nblock-th integration from rfifind mask
        if self.rfifind.mask(Nblock).size > 0:
            masked_data[*self.rfifind.mask(Nblock)] = np.ma.masked

        ## Print progress
        flag_chan_count = self.nfchan(masked_data) - self.N_flagged_chan
        logger.info("        DONE: %i channels were zapped according to the given rfifind mask!" % flag_chan_count)
        self.N_flagged_chan += flag_chan_count
        self.N_flagged_samp += flag_chan_count * masked_data.shape[1]

        return masked_data

    def zap_overpowered(self,masked_data,threshold=None):
        """
        Zap entire channel if chan pow > threshold*rms.
        """
        if threshold is None:
            threshold = 6. # Default value

        while True:
            spec = np.ma.mean(masked_data,axis=1)
            ## Get statistics
            # Median
            med0 = self.bn_median(spec)
            spec_norm = spec-med0
            max_offset = np.ma.max(np.ma.sort(abs(spec_norm))[:-int(0.15*len(spec_norm))]) # Trow away the 15% biggest offset data points
            spec_norm = spec_norm[abs(spec_norm) < max_offset] # Remove the 15% largest spikes from normalised channels
            med = self.bn_median(spec_norm)+med0
            # Standard deviation
            rms = self.bn_std(spec) #Use spec, incase to many high data points have been thrown away (more conservative)

            ## Threshold all channels
            to_zap = np.argwhere(abs(spec-med) > threshold*rms)

            if len(to_zap) == 0:
                break

            ## Update mask
            masked_data[to_zap] = np.ma.masked

        ## Print progress
        flag_chan_count = self.nfchan(masked_data) - self.N_flagged_chan
        logger.info("        DONE: all channels were thresholded with a threshold of %0.2f." % threshold)
        logger.info("              %i channels have been zapped." % flag_chan_count)
        self.N_flagged_chan += flag_chan_count
        self.N_flagged_samp += flag_chan_count * masked_data.shape[1]

        return masked_data


    #################################
    # Time domain functions
    #################################

    def TS_threshold(self,masked_data,threshold=None):
        """
        Flag entire spectrum (for one time sample) if TS > threshold*rms.
        """
        if threshold is None:
            threshold = 4. # Default value

        while True:
            TS = np.ma.mean(masked_data,axis=0) # Use mean to mitigate the effects of flagged channels
            ## Get statistics
            # Median
            med0 = self.bn_median(TS)
            TS_norm = TS-med0
            max_offset = np.ma.max(np.ma.sort(abs(TS_norm))[:-int(0.15*len(TS_norm))]) # Trow away the 15% biggest offset data points
            TS_norm = TS_norm[abs(TS_norm) < max_offset] # Remove the 15% largest spikes from normalised time series
            med = self.bn_median(TS_norm)+med0
            # Standard deviation
            rms = self.bn_std(TS) #Use TS, incase to many high data points have been thrown away (more conservative)

            ## Threshold the TS
            to_flag = np.argwhere(abs(TS-med) > threshold*rms)
            to_flag = np.append(np.zeros_like(to_flag),to_flag,axis=1).T

            if to_flag.shape[1] == 0:
                break

            if self.edge:
                ## Flag edges of thresholded RFI
                to_flag_ext = self.add_edges(to_flag,masked_data.shape[0],masked_data.shape[1],1)
            else:
                ## Do not flag the edges
                to_flag_ext = to_flag

            ## Update mask
            masked_data[:,to_flag_ext[1]] = np.ma.masked

        ## Print progress
        flag_spec_count = self.nfspec(masked_data) - self.N_flagged_spec
        logger.info("        DONE: time series has been thresholded with a threshold of %0.2f." % threshold)
        logger.info("              %i full spectra have been flagged." % flag_spec_count)
        self.N_flagged_spec += flag_spec_count
        self.N_flagged_samp += flag_spec_count * masked_data.shape[0]

        return masked_data

    def rebin1D(self, arr, binf):
        """
            arr  = numpy masked array!
            binf = factor to bin scrunch with
        """
        logger.info(f"        Downsampling with a factor of {binf} in time.")
        sh = arr.shape[0],arr.shape[1]//binf,binf
        return np.ma.mean(arr.reshape(sh),axis=-1)

    def TS_threshold_downsampled(self,masked_data,threshold=None,downsampfrac=None):
        """
        Flag entire downsampled spectrum (for one time sample) if TS > threshold*rms.
        """
        if threshold is None:
            threshold = 4. # Default value

        if downsampfrac is None:
            downsampfrac = 2**3
        else:
            downsampfrac = 2**downsampfrac

        # Convert downsampling factor to an integer
        downsampfrac = int(downsampfrac)

        mod = (masked_data.shape[1] % downsampfrac)
        if mod != 0:
            # Append spectra with median values to enable rebinning
            med  = self.bn_median(masked_data,axis=1)
            add = (masked_data.shape[1]//downsampfrac + 1)*downsampfrac-masked_data.shape[1]
            ones = np.ones((masked_data.shape[0],add))
            TS = np.ma.mean(self.rebin1D(np.append(masked_data,ones*med[:,None],axis=1),downsampfrac),axis=0)
        else:
            add = 0
            TS = np.ma.mean(self.rebin1D(masked_data,downsampfrac),axis=0) # Use mean to mitigate the effects of flagged channels

        ## Get statistics
        # Median
        med0 = self.bn_median(TS)
        TS_norm = TS-med0
        max_offset = np.ma.max(np.ma.sort(abs(TS_norm))[:-int(0.15*len(TS_norm))]) # Trow away the 15% biggest offset data points
        TS_norm = TS_norm[abs(TS_norm) < max_offset] # Remove the 15% largest spikes from normalised time series
        med = self.bn_median(TS_norm)+med0
        # Standard deviation
        rms = self.bn_std(TS) #Use TS, incase to many high data points have been thrown away (more conservative)

        ## Threshold the TS
        to_flag = np.argwhere(abs(TS-med) > threshold*rms)
        to_flag = np.arange(masked_data.shape[1] + add).reshape(-1,downsampfrac)[to_flag.flatten()].flatten().reshape(-1,1)
        to_flag = np.append(np.zeros_like(to_flag),to_flag,axis=1).T

        if self.edge:
            ## Flag edges of thresholded RFI
            to_flag_ext = self.add_edges(to_flag,masked_data.shape[0],masked_data.shape[1],1)
        else:
            ## Do not flag the edges
            to_flag_ext = to_flag

        ## Update mask
        masked_data[:,to_flag_ext[1]] = np.ma.masked

        ## Print progress
        flag_spec_count = self.nfspec(masked_data) - self.N_flagged_spec
        logger.info("        DONE: DOWNSAMPLED time series has been thresholded with a threshold of %0.2f and a factor %i." % (threshold,downsampfrac))
        logger.info("              %i full spectra have been flagged." % flag_spec_count)
        self.N_flagged_spec += flag_spec_count
        self.N_flagged_samp += flag_spec_count * masked_data.shape[0]

        return masked_data

    def std_threshold(self,masked_data,threshold=None):
        """
        Flag entire spectrum (for one time sample) if std(data block) > threshold*rms.
        """
        if threshold is None:
            threshold = 8. # Default value

        while True:
            STD = self.bn_std(masked_data,axis=0)
            ## Get statistics
            # Median
            med0 = self.bn_median(STD)
            STD_norm = STD-med0
            max_offset = np.ma.max(np.ma.sort(abs(STD_norm))[:-int(0.15*len(STD_norm))]) # Trow away the 15% biggest offset data points
            STD_norm = STD_norm[abs(STD_norm) < max_offset] # Remove the 15% largest spikes from normalised time series
            med = self.bn_median(STD_norm)+med0
            # Standard deviation
            rms = self.bn_std(STD) #Use TS, incase to many high data points have been thrown away (more conservative)

            ## Threshold the standard deviation
            to_flag = np.argwhere(abs(STD-med) > threshold*rms)
            to_flag = np.append(np.zeros_like(to_flag),to_flag,axis=1).T

            if to_flag.shape[1] == 0:
                break

            if self.edge:
                ## Flag edges of thresholded RFI
                to_flag_ext = self.add_edges(to_flag,masked_data.shape[0],masked_data.shape[1],1)
            else:
                ## Do not flag the edges
                to_flag_ext = to_flag

            ## Update mask
            masked_data[:,to_flag_ext[1]] = np.ma.masked

        ## Print progress
        flag_spec_count = self.nfspec(masked_data) - self.N_flagged_spec
        logger.info("        DONE: standard deviation of data BLOCK  has been thresholded with a threshold of %0.2f." % threshold)
        logger.info("              %i full spectra have been flagged." % flag_spec_count)
        self.N_flagged_spec += flag_spec_count
        self.N_flagged_samp += flag_spec_count * masked_data.shape[0]

        return masked_data

    def chan_threshold(self,masked_data,threshold=None,int_extr=2.5):
        """
        Remove outliers > threshold*rms from each channel in masked_data.
        OR (for int data) remove outliers at edge of dtype range.
        """

        ## Determine type of data
        if (self.dtype.startswith('uint')) and (threshold is None):
            ## In case normalised data is given, re-add med and std to find data extremeties
            if self.norm_data:
                orig_data = np.ma.array(masked_data.data*self.std0[:,None] + self.med0[:,None], mask=masked_data.mask)
            else:
                orig_data = masked_data

            # Loop over each channel and flag dtype extremities
            dtype_range = self.idtype.max - self.idtype.min
            dflag = int_extr/100. * dtype_range # Remove the int_extr%% data extremities

            # Find extremities
            #to_flag = np.argwhere((masked_data < self.idtype.min+dflag) | (masked_data > self.idtype.max-dflag))
            to_flag = np.argwhere(orig_data.data > self.idtype.max-dflag).T # Only remove too high values!!!

            if self.edge:
                # Flag edges of extremities in time
                to_flag_ext = self.add_edges(to_flag,masked_data.shape[0],masked_data.shape[1],1)
                # Flag edges of extremities in frequency
                to_flag_ext = self.add_edges(to_flag_ext,masked_data.shape[0],masked_data.shape[1],0)
            else:
                # Do not flag edges
                to_flag_ext = to_flag
                
            # Add to mask
            masked_data[to_flag_ext[0],to_flag_ext[1]] = np.ma.masked

            # Print progress
            flag_samp_count = self.nfsamp(masked_data) - self.N_flagged_samp
            logger.info("        DONE: dtype extrimities have been removed from the individual channels.")
            logger.info("              %i samples were flagged." % flag_samp_count)
            self.N_flagged_samp += flag_samp_count

        else:
            if threshold is None:
                threshold = 10. # Default for float data

            # Calculate statistics
            med = self.bn_median(masked_data,axis=1)
            rms = self.bn_std(masked_data,axis=1)

            # Threshold channels
            to_flag = np.array(np.where(abs(masked_data.data-med[:,None]) > threshold*rms.filled(self.idtype.max)[:,None]))

            if self.edge:
                # Flag edges of thresholded RFI in time
                to_flag_ext = self.add_edges(to_flag,masked_data.shape[0],masked_data.shape[1],1)
                # Flag edges of thresholded RFI in frequency
                to_flag_ext = self.add_edges(to_flag_ext,masked_data.shape[0],masked_data.shape[1],0)
            else:
                # Do not flag edges
                to_flag_ext = to_flag

            # Add to mask
            masked_data[to_flag_ext[0],to_flag_ext[1]] = np.ma.masked

            # Print progress
            flag_samp_count = self.nfsamp(masked_data) - self.N_flagged_samp
            logger.info("        DONE: individuel channels have been thresholded, with a threshold of %0.2f." % threshold)
            logger.info("              %i samples were flagged." % to_flag_ext.shape[1])
            self.N_flagged_samp += flag_samp_count

        return masked_data

    def TS_remove_regular(self, masked_data, start_spec=0, period=None):
        """
        Remove periodic RFI in the time series.
        NOTE: the period has two special values:
              1) None; which lets the user select the period from an inspectra instance
              2) 0;    which makes this function search for a period between 1 and 2**8 samples
              3) >0;   which removes periodic regions with the specified period (in samples)
        """
        loop = True
        period_list  = []
        while True:
            ## Apply regular RFI identification on original data
            if self.norm_data:
                orig_data = np.ma.array(masked_data*self.std0[:,None] + self.med0[:,None], mask=masked_data.mask)
            else:
                orig_data = masked_data

            ## Get time series
            TS = np.ma.mean(orig_data,axis=0) * masked_data.shape[0] # Use mean here in order to mitigate the effects of flagged channels
            med = self.bn_median(TS)

            ## Get period of regularity
            if period is None:
                # Show spectra and ask if block contains the regularity to remove
                inspec_plot = inspectra.inspectra(orig_data, start_nspec=start_spec, select_lim=2, \
                                    title="Does this BLOCK contain the regularity that you want to remove?\nIf so, press 'p' and indicate the period by <click> and <press 't'> twice in the time-series.")
                inspec_out  = inspec_plot()
                del inspec_plot # Free memory

                if len(inspec_out) == 1:
                    # BLOCK does not contain regularity to remove
                    self.inspec = False
                    ## Print progress
                    logger.info("        DONE: NO regular regions were removed.")
                    return masked_data
                else:
                    # BLOCK contains regularity to remove
                    self.inspec = True
                    self.rm_reg = pperiod = int(inspec_out[1][1,0].round() - inspec_out[1][0,0].round()) # In number of samples

                # Do not loop
                loop = False

            elif period == 0:
                ## Auto find periods in data
                cross_sum = []
                for per in range(1,2**8):
                    cross_corr = sp.signal.correlate(TS-med, np.ones(per), mode='same') / per
                    zeros = np.argwhere(np.diff(cross_corr) == 0)
                    cross_sum.append(len(zeros))

                # Determine the std without the large peaks from regular RFI regions
                cross_sum_norm = cross_sum-np.median(cross_sum)
                max_offset = np.max(np.sort(abs(cross_sum_norm))[:-int(0.2*len(cross_sum_norm))]) # Throw away the 20% highest samples
                cross_sum_norm = cross_sum_norm[abs(cross_sum_norm) <= max_offset]
                # Find peaks that have an SNR above 10
                peaks = np.argwhere(abs(cross_sum-np.median(cross_sum)) > 10*np.std(cross_sum_norm)).flatten()
                properties = abs(cross_sum-np.median(cross_sum))[peaks]

                if len(peaks) == 0:
                    # Do not loop
                    loop = False
                    break
                else:
                    pperiod = peaks[properties.argmax()] + 1 #Adding one due to the use of np.diff in the for loop above

            else:
                pperiod = period
                # Do not loop
                loop = False

            ## Collect periods that are removed
            period_list.append(pperiod)

            ## Get cross correlation with a boxcar of width period
            cross_corr = sp.signal.correlate(TS-med, np.ones(pperiod), mode='same') / pperiod

            ## Get indices of regular regions
            idx_zero = np.argwhere(np.diff(cross_corr).astype(int) == 0).flatten()
            idx_flat = np.split(idx_zero, np.where(np.diff(idx_zero) != 1)[0]+1)
            to_flag  = np.array([],dtype=int)
            for flat in idx_flat:
                if len(flat) > pperiod:
                    to_flag = np.append(to_flag, flat)

            if len(to_flag) == 0:
                # quite looping
                logger.warning("\nWARNING: a found periodicity can not be removed...")
                loop  = False
                break

            ## Ensure edges around regularity are flagged
            edge = max(pperiod, self.edge) # Remove at least period number of samples, since the cross correlation does not detect partial repations at the edges

            ## Flag the edges of the regular regions
            to_flag = np.append(np.zeros_like(to_flag).reshape(-1,1),to_flag.reshape(-1,1),axis=1).T
            to_flag_ext = self.add_edges(to_flag,masked_data.shape[0],masked_data.shape[1],1,edge)

            ## Update mask
            masked_data[:,to_flag_ext[1]] = np.ma.masked

            if not loop:
                break

        ## Print progress
        flag_spec_count = self.nfspec(masked_data) - self.N_flagged_spec
        logger.info(f"        DONE: regular regions with a period of {period_list} samples have been removed.")
        logger.info(f"              {flag_spec_count} spectra were flagged.")
        self.N_flagged_spec += flag_spec_count
        self.N_flagged_samp += flag_spec_count * masked_data.shape[0]

        return masked_data

    def smooth_time_flagging(self,masked_data,percent):
        ## Get indices of spectra with more than {percent} flagged channels
        to_flag = np.argwhere(masked_data.mask.mean(axis=0) > (percent/100)).flatten()

        # Update mask
        masked_data[:,to_flag] = np.ma.masked

        ## Get width of fully flagged spectra ranges and flag the inbetween ranges if their widths are smaller
        for _ in range(3): # Repeat 3 times to truly remove all 'inbetween' ranges
            TS_fully_flagged = np.all(masked_data.mask, axis=0)
            flag_specs = np.where(np.diff(TS_fully_flagged))[0]
            if TS_fully_flagged[0]:
                flag_specs = np.insert(flag_specs,0,0)
            if TS_fully_flagged[-1]:
                flag_specs = np.append(flag_specs,len(TS_fully_flagged))

            # Determine widths of flagged regions
            flag_widths = flag_specs[1::2] - flag_specs[::2]
            flag_idx    = flag_specs[::2] + 1
            # Determine widths of valid regions
            flag_specs  = np.append(flag_specs,len(TS_fully_flagged))
            val_widths  = flag_specs[2::2] - flag_specs[1::2]
            val_idx     = flag_specs[1::2] + 1

            # In order to minimise edge effects do not flag unflagged spectra at edges
            val_widths = val_widths[:-1]
            val_idx    = val_idx[:-1]

            for idx in range(0,len(val_widths)):
                av_width = (flag_widths[idx]+flag_widths[idx+1])//2
                if val_widths[idx] < av_width:
                    # Update mask
                    masked_data[:,val_idx[idx]:val_idx[idx]+val_widths[idx]] = np.ma.masked

        ## Print progress
        flag_spec_count = self.nfspec(masked_data) - self.N_flagged_spec
        logger.info("        DONE: spectra were smoothed.")
        logger.info("              %i spectra are now additionaly flagged." % flag_spec_count)
        self.N_flagged_spec += flag_spec_count
        self.N_flagged_samp += flag_spec_count * masked_data.shape[0]

        return masked_data

    def smooth_freq_zapping(self, masked_data, percent):
        ## Get indices of channels with more than {percent} flagged spectra
        to_flag = np.argwhere(masked_data.mask.mean(axis=1) > (percent/100)).flatten()

        # Update mask
        masked_data[to_flag] = np.ma.masked

        ## Print progress
        flag_chan_count = self.nfchan(masked_data) - self.N_flagged_chan
        logger.info(f"        DONE: channels zapped with more than {percent}% spectra flagged.")
        logger.info("              %i channels have now been additionaly zapped." % flag_chan_count)
        self.N_flagged_chan += flag_chan_count
        self.N_flagged_samp += flag_chan_count * masked_data.shape[1]

        return masked_data

    def zero_DMing(self, data):
        #https://doi.org/10.1111/j.1365-2966.2009.14524.x
        data_mean = np.mean(data,axis=0)

        if self.dtype.startswith('uint'):
            data -= data_mean
            # Move mean of spectra to mean of dtype range
            dtype_med = (self.idtype.max - self.idtype.min) / 2
            data += dtype_med
        else:
            data -= data_mean

        # Print progress
        logger.info("        DONE: zero-DM filter applied.")

        return data


def main():
    ### Initialise Filterbank looper
    if args.outname is None:
        args.outname = args.infile.split('.fil')[0]+'_MASKED.fil'
    Floop = fil_looper.looper(args.infile,args.outname,args.BLOCK,args.ncores,args.verbose)

    ### Initialise mask maker
    if args.write_mask is not False:
        if args.write_mask is None:
            args.write_mask = args.infile.split('.fil')[0]

        fil_details = {'MJD'   : Floop.rfil.tstart, \
                       'dtint' : Floop.rfil.tsamp, \
                       'fch1'  : Floop.rfil.fch1, \
                       'df'    : Floop.rfil.foff, \
                       'nchan' : Floop.rfil.nchan, \
                       'nint'  : Floop.rfil.nspec}
        rfibyemask = mask_maker.masker(args.write_mask,**fil_details)
        args.write_mask = rfibyemask

    ### Remove unnecessary arguments
    del args.infile
    del args.outname
    del args.BLOCK
    del args.ncores

    ### Initialise RFI mitigator
    flag_params = vars(args)
    flag_params.update({'dtype' : Floop.rfil.dtype})
    rm_RFI = RFI_mitigator(**flag_params)

    # Check if possible usage of rfifind mask implies a different BLOCK size
    if hasattr(rm_RFI,'rfifind'):
        Floop.BLOCK = rm_RFI.rfifind.ptsperint
        logger.warning("\nBLOCK size was automatically changed to %i spectra, due to the application of an rfifind mask. " % Floop.BLOCK)

    if args.write_mask:
        # Set block size of mask maker
        rfibyemask.block_size = Floop.BLOCK

    ## Start RFI removal
    Floop.start_loop(rm_RFI.clean)

    # Print flag stats
    logger.info("\nALL DONE")
    if Floop.ncores is False:
        logger.info("%.2f%% of data samples have been masked with Gaussian noise." % \
                   (100.0*rm_RFI.N_flagged_samples / (Floop.rfil.nchan*Floop.rfil.nspec)))

    return


if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='RFIbye.py', \
                                     description='RFI mitigation code that removes RFI infected data elements with random Gaussian noise.')

    parser.add_argument("infile", \
                            help="Filterbank file that needs some RFI blasted away.")
    parser.add_argument("-n", "--norm_data", action='store_true', default=False, \
                            help="Identify RFI using normalised data [(data - med) / std]")
    parser.add_argument("--ts_thres", nargs='?', type=float, default=False, metavar='SNR', \
                            help="Flag entire spectra whenever time series exceeds the set SNR.")
    parser.add_argument("--ts_downsampfrac", nargs='?', type=float, default=False, metavar='2**pow', \
                            help="Threshold the time series again with the set 'ts_thres' threshold but after it has been downsampled with the given power of 2 in time.")
    parser.add_argument("--std_thres", nargs='?', type=float, default=False, metavar='SNR', \
                            help="Flag entire spectra whenever its standard deviation exceeds the set SNR.")
    parser.add_argument("--chan_thres", nargs='?', type=float, default=False, metavar='SNR', \
                            help="Flag time,frequency sample whenever a sample exceeds the set SNR in a specific channel.")
    parser.add_argument("--chan_int_extr_thres", nargs=1, type=float, default=2.5, metavar='percent', \
                            help="When channel thresholding integer data, remove this percentage of the max dtype range instead of using the SNR.")
    parser.add_argument("--edge", type=int, default=0, \
                            help="Set how many samples need to be flagged around a classified RFI sample as well.")
    parser.add_argument("--full_spec_flag", type=int, default=30, metavar='percent',\
                            help="Set the percentage of zapped channels above which a spectrum will be fully flagged.")
    parser.add_argument("--rm_reg", nargs='?', type=int, default=False, metavar='period', \
                            help="Flag potential regularities in the time series with the given period (in samples).")
    parser.add_argument("--rm_band_edge", nargs='?', type=float, default=False, metavar='percent', \
                            help="Remove the given percentage of the top and bottom of the band. If no percentage is given default is remove 1%% of band edges.")
    parser.add_argument("--rm_bandpass", action='store_true', default=False, \
                            help="Subtract bandpass median and devide by data standard deviation.")
    parser.add_argument("--chan_indep", action='store_true', default=False, \
                            help="When subtracting bandpass, devide by standard deviation of each channel instead of entire data BLOCK.")
    parser.add_argument("--zap_chans", type=str, default=None, metavar='chan numbers', \
                            help="Comma seperated list (no spaces!) of channel numbers to flag, where channel 0 is the highest frequency channel.")
    parser.add_argument("--zap_chan_thres", nargs='?', type=float, default=False, metavar='SNR', \
                            help="Flag a channel whenever its power exceeds the set SNR.")
    parser.add_argument("--full_chan_zap", type=int, default=20, metavar='percent',\
                            help="Set the percentage of flagged spectra above which a channel will be fully zapped.")
    parser.add_argument("--rfifind_mask", type=str, default=None, metavar='Path to file', \
                            help="PRESTO rfifind mask to apply to data.")
    parser.add_argument('--zerodm', action='store_true', default=False, \
                            help="Apply zero-DM filter to data to (hopefully) remove broad-band RFI.")
    parser.add_argument("--block_size", dest='BLOCK', type=int, default=2**15, \
                            help="Number of spectra per read. This is the amount of data processed at a time.")
    parser.add_argument("--write_mask", nargs='?', type=str, default=False, metavar='Mask file name', \
                            help="Write the flagged samples to an RFIbye mask.")
    parser.add_argument('--inspec', action='store_true', default=False, \
                            help="Show GUI to inspect the quality of the applied mask, per iteration.")
    parser.add_argument("-j", "--ncores", nargs='?', type=int, default=False,
                            help="Number of cores to use for processing.")
    parser.add_argument("-o", "--outname", dest='outname', default=None,
                            help="Name of output file.")
    parser.add_argument("-v", "--verbose", action='store_true', default=False, \
                            help="Print more operation details")

    args = parser.parse_args()
    do_clean = (args.ts_thres is not False) | (args.std_thres is not False) | (args.chan_thres is not False) | (args.rm_reg is not False) | \
               (args.rm_reg is not False) | (args.rfifind_mask is not None) | args.zerodm | (args.rm_band_edge is not False) | \
               args.rm_bandpass | (args.zap_chans is not None) | (args.zap_chan_thres is not False)
    if not do_clean:
        log_str = "\nNothing to be done here...\nSpecify how RFI should be removed."
        logger.error(log_str)
        raise SystemExit(log_str)

    if (args.ts_thres is False) and (args.ts_downsampfrac is not False):
        log_str = "\nOne must enable time series thresholding, before it is possible to threshold the downsampled time series!"
        logger.error(log_str)
        raise SystemExit(log_str)

    if (args.ncores is not False) and (args.rm_reg is None):
        logger.warning("\nMultiprocessing mode enabled")
        logger.warning("    It is not possible to select a period from an inspectra plot, now NO regularities are removed...")
        args.rm_reg = False
    elif (args.ncores is not False) and (args.write_mask is not False):
        logger.warning("\nMultiprocessing mode enabled")
        logger.warning("    It is not possible to write mask to file now, NO mask file will be produced...")
        args.write_mask = False
    elif (args.ncores is not False) and (args.inspec is True):
        logger.warning("\nMultiprocessing mode enabled")
        logger.warning("    It is not possible to inspect spectra in multiprocessing mode, turning this function off...")
        args.inspec = False
        

    #Configure logger
    set_logging.main(args.infile.split('.fil')[0],args.verbose)

    main()
