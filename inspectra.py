#!/usr/bin/env python

"""
inspectra.py

Inspect the spectra of a Filterbank file.
Either the raw data can be looked at or one can load
an RFIbye mask to see what parts of a data file have
been interchanged with random values.

Leon Houben
"""

import os,sys
import argparse
import fil_looper
import mask_maker
import set_logging
import logging
import copy
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

logger = logging.getLogger(__name__)


class inspectra(object):
    """Inspect spectra data
       A zoomed part of the loaded spectrum is shown through which one can pan with the '<' and '>' keys.
       This allows for better inspection of the data and determination of the quality of the mask made.
    """
    def __init__(self,masked_data,start_nspec,tsamp=None,fch1=None,foff=None, \
                      rm_bandpass=False,pchan=False,select_lim=None,title=None):
        """inspectra constructor

            Input:
                masked_data: A 2D masked numpy array containing the data to be inspected
                start_nspec: Spectrum number corresponding to the first spectra in masked_data.
                tsamp:       Sampling time (seconds) of the data.
                fch1:        Frequency (MHz) of the first channel in masked_data.
                foff:        Channel bandwidth in MHz.
                rm_bandpass: If True, the bandpass is removed before plotting.
                pchan:       If True, each channel is devided by its std when rm_bandpass is True.
                select_lim:  Defines how many points can be selected whenever picking is activated.
                title:       Possible title to be displayed on the plot window.

            Output:
                inspectra object
        """
        logger.info("    INSPECTRA instance launched...")

        ## Set class property
        self.continue_plotting = True
        self.select_limit = select_lim

        ## Build figure
        self.fig  = plt.figure(figsize=(17,5))
        gs        = self.fig.add_gridspec(nrows=5, ncols=17)
        self.dyn  = self.fig.add_subplot(gs[1:,:-1])
        self.ts   = self.fig.add_subplot(gs[0,:-1], sharex=self.dyn)
        self.spec = self.fig.add_subplot(gs[1:,-1], sharey=self.dyn)
        self.fig.subplots_adjust(left=0.05, right=0.95, bottom=0.15, top=0.95)
        self.mlab = self.fig.text(0.9,0.025,"Mask: off")
        if title is not None:
            self.fig.suptitle(title, fontsize=12)
        self.xlabel = "Sample number"
        self.ylabel = "Channel number"
        # Zoom/Pan settings
        self.Llim = None
        self.Rlim = None
        self.Tlim = None
        self.Blim = None
        self.TSt  = 12.
        self.TSb  = -6.

        ## Set data specifics
        self.data  = masked_data
        self.mask  = False
        self.nchan, self.nspec = self.data.data.shape
        self.times = np.arange(start_nspec,start_nspec+self.nspec,1).astype(int)
        self.freqs = np.arange(self.nchan)
        if tsamp is not None:
            self.times = self.times*tsamp
            self.xlabel = "Time (in seconds into file)"
        if (fch1 is not None) and (foff is not None):
            self.freqs = fch1 + foff*self.freqs
            self.ylabel = "Frequency (in MHz)"
        # Time selection parameters
        self.tblock = np.minimum(int(4*self.nchan), self.nspec-1)
        self.t_st  = 0 # Start time of plot
        self.t_end = self.tblock # End time of plot
        # Possible bandpass removel
        if rm_bandpass:
            self.data = np.ma.array(self.remove_bandpass(np.ma.getdata(self.data),pchan),mask=self.data.mask)
        # Data statistics
        self.TS_mask     = self.data.mean(0) # Mean is used, instead of sum, to mitigate effects of flagged channels
        self.TS_mask_med = np.ma.median(self.TS_mask)
        self.TS_mask_std = np.ma.std(self.TS_mask)
        self.TS_orig     = np.mean(self.data.data,axis=0) # Mean is used, instead of sum, to mitigate effects of flagged channels
        self.TS_orig_med = np.median(self.TS_orig)
        self.TS_orig_std = np.std(self.TS_orig)

        self.max = np.max(self.data)
        self.min = np.min(self.data)

        ## Populate axis
        self.populate_axis()

        ## Activate interactive picking
        self.fig.canvas.mpl_connect('key_press_event', self.onpress)

        plt.show()

    def __call__(self):
        if hasattr(self, 'xydata'):
            return (self.continue_plotting, self.xydata)
        else:
            return (self.continue_plotting, )

    def populate_axis(self):
        ## Plot time series and spectrum
        self.ts.cla()
        self.spec.cla()
        self.ts.set_ylabel("SNR")
        if self.mask:
            self.ts.plot(self.times[self.t_st:self.t_end],(self.TS_mask[self.t_st:self.t_end]-self.TS_mask_med)/self.TS_mask_std)
            self.spec.plot(np.sum(self.data[:,self.t_st:self.t_end],axis=1)/self.tblock,self.freqs)
        else:
            self.ts.plot(self.times[self.t_st:self.t_end],(self.TS_orig[self.t_st:self.t_end]-self.TS_orig_med)/self.TS_orig_std)
            self.spec.plot(np.sum(self.data.data[:,self.t_st:self.t_end],axis=1)/self.tblock,self.freqs)

        ## Plot dynamic spectrum
        self.dyn.cla()
        self.dyn.set_ylabel(self.ylabel)
        self.dyn.set_xlabel(self.xlabel)
        # Plot data
        if self.mask:
            ccmap = copy.copy(mpl.cm.get_cmap())
            ccmap.set_bad(color='red')
            self.dyn.imshow(self.data.filled(np.nan)[:,self.t_st:self.t_end], aspect="auto", \
                            extent=(self.times[self.t_st],self.times[self.t_end],self.freqs[-1],self.freqs[0]), \
                            vmin=self.min, vmax=self.max, cmap=ccmap)
            self.mlab.set_text("Mask: on")
        else:
            self.dyn.imshow(self.data.data[:,self.t_st:self.t_end], aspect="auto", \
                            extent=(self.times[self.t_st],self.times[self.t_end],self.freqs[-1],self.freqs[0]), \
                            vmin=self.min, vmax=self.max)
            self.mlab.set_text("Mask: off")

        ## Preserve zoom settings
        self.ts.set_ylim(self.TSb,self.TSt)
        self.dyn.set_xlim(self.Llim,self.Rlim)
        self.dyn.set_ylim(self.Blim,self.Tlim)

        ## Make plot fancy
        plt.setp(self.ts.get_xticklabels(), visible=False)
        plt.setp(self.spec.get_yticklabels(), visible=False)

    def remove_bandpass(self,data,indep=False):
        """Subtract the median from each channel,
           and divide by global std deviation (if indep==False), or
           divide by std deviation of each channel (if indep==True).

           Input:
               data:  Numpy array of data that needs its bandpass removed.
               indep: Boolean. If True, scale each channel independantly (Default: False).

           Output:
               scaled_date: The scaled data.

           Note: taken from PRESTO's spectra.py
        """
        if not indep:
            std = data.std()
        for ii in range(data.shape[0]):
            chan = data[ii,:]
            median = np.median(chan)
            if indep:
                std = np.array([chan.std()])
                std = np.where(std==0,1,std)[0]
            chan[:] = (chan-median)/std
        return data

    def onpress(self, event):
        if event.key not in (',', '.', 'c', 'm', '/','[',']','enter','d','p','n','t','q'):
            return True
        if event.key == '.': #Show next block of data
            self.t_st  += self.tblock
            self.t_end += self.tblock
            self.newW   = True
        elif event.key == ',': #Show previous block of data
            self.t_st  -= self.tblock
            self.t_end -= self.tblock
            self.newW   = True
        elif event.key == 'm': #Show first block of data (start of data)
            self.t_st  = 0
            self.t_end = self.tblock
            self.newW  = True
        elif event.key == '/': #Show last block of data (end of data)
            self.t_st  = int(self.nspec-self.tblock)
            self.t_end = self.nspec
            self.newW  = True
        elif event.key == 'c': #Show centre of data
            self.t_st  = int(self.nspec//2 - self.tblock//2)
            self.t_end = int(self.nspec//2 + self.tblock//2)
            self.newW  = True
        elif event.key == '[': # Toggle mask view off
            self.mask = False
            self.newW = False
        elif event.key == ']': # Toggle mask view on
            self.mask = True
            self.newW = False
        elif event.key == 'q':
            self.continue_plotting = False
            plt.close()
            return
        elif event.key == 'd':
            self.continue_plotting = False
            plt.close()
            return
        elif event.key == 'enter':
            self.continue_plotting = True
            plt.close()
            return
        elif event.key == 'n':
            self.continue_plotting = True
            plt.close()
            return
        elif event.key == 'p':
            self.continue_plotting = False
            # Activate interactive picking
            self.xydata = np.array([]).reshape(-1,2)
            self.fig.canvas.mpl_connect('button_press_event', self.onclick)
            self.fig.suptitle("PICKING ACTIVATED; select data point and take its value with pressing 't'.", fontsize=12)
            self.fig.canvas.draw()
            return
        elif event.key == 't':
            if hasattr(self, 'axdata'):
                self.xydata = np.append(self.xydata,self.axdata.reshape(-1,2),axis=0)
            else:
                print("Select a data point in one of the axis before pressing 't'.")

            if len(self.xydata) == self.select_limit:
                plt.close()

            return

        # Clip time values so data slicing does not go out of bounds
        self.t_st  = np.clip(self.t_st, 0, self.nspec-self.tblock-1)
        self.t_end = np.clip(self.t_end, self.tblock, self.nspec-1)
        self.reload()

    def set_limits(self,new=False):
        if new:
            self.ts.set_ylim(auto=True)
            self.dyn.set_ylim(auto=True)
            self.dyn.set_xlim(auto=True)

            self.TSb, self.TSt = -6., 12.
            self.Llim = self.Rlim = self.Blim = self.Tlim = None
        else:
            self.TSb, self.TSt    = self.ts.get_ylim()
            self.Llim, self.Rlim = self.dyn.get_xlim()
            self.Blim, self.Tlim = self.dyn.get_ylim()

    def onclick(self, event):
        if event.inaxes != self.ts: return
        ## Set clicked data params
        self.axdata = np.array([event.xdata,event.ydata])

    def reload(self):
        ## Set Zoom/Pan settings
        self.set_limits(self.newW)

        self.populate_axis()
        self.fig.canvas.draw()


def inspec_plot(spectra,Nblock,**kwargs):
    # Convert data into masked data instance
    if kwargs['mask'] is not None:
        masked_data = np.ma.array(spectra,mask=kwargs['mask'].mask(Nblock),fill_value=0)
    else:
        masked_data = np.ma.array(spectra,fill_value=0)

    # Launch inspectra
    logger.info("    Showing data %.3f seconds into file." % (kwargs['nspec']*kwargs['tsamp']))
    continue_plot = inspectra(masked_data,kwargs['nspec'],tsamp=kwargs['tsamp'],fch1=kwargs['fch1'],foff=kwargs['foff'], \
                              rm_bandpass=kwargs['rm_band'],pchan=kwargs['chan_indep'])
    if not continue_plot()[0]:
        sys.exit(0)

    return

def main():
    # Initialise Filterbank looper
    logger.info("\nSetting up the Filterbank handler.")
    Floop = fil_looper.looper(args.infile,False,args.BLOCK,args.verbose)
    if args.time is not None:
        args.BLOCK  = int(np.ceil(args.time/Floop.rfil.tsamp))
        Floop.BLOCK = args.BLOCK

    # Determine where to start plotting
    nstart = int(np.floor(args.start/Floop.rfil.tsamp))

    # Load potentially given RFIbye mask
    if args.rfibye_mask is not None:
        logger.info("\nPlotting with a mask.")
        masker = mask_maker.masker(args.rfibye_mask)
        masker.block_size  = args.BLOCK
        masker.ispec_start = nstart
    else:
        logger.info("\nPlotting without a mask.")
        masker = None

    # Start plotting
    Floop.start_loop(inspec_plot,start_nspec=nstart,mask=masker,rm_band=args.rm_bandpass,chan_indep=args.chan_indep)

    return


if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='inspectra.py', formatter_class=argparse.ArgumentDefaultsHelpFormatter, \
                                     description='Filterbank data plotter to inspect its spectra.')

    parser.add_argument("infile", \
                            help="Filterbank file of which you want to inspect its spectra.")
    parser.add_argument("--rfibye_mask", type=str, default=None, metavar='Path to file', \
                            help="Show RFIbye mask to determine its quality.")
    parser.add_argument("--rm_bandpass", action='store_true', default=False, \
                            help="Remove data's bandpass before plotting.")
    parser.add_argument("--chan_indep", action='store_true', default=False, \
                            help="Devide by std of each channel (instead of global std) when removing bandpass.")
    parser.add_argument("-S", "--start", type=float, default=0, \
                            help="Number of seconds into data to start plotting spectra.")
    parser.add_argument("-T", "--time", type=float, default=None, \
                            help="Duration (in seconds) of the extracted data per read. Takes precedence over the 'block-size'.")
    parser.add_argument("-B", "--block-size", dest='BLOCK', type=int, default=2**13, \
                            help="Number of spectra per read.")
    parser.add_argument("-v", "--verbose", action='store_true', default=False, \
                            help="Print more operation details")

    args = parser.parse_args()

    #Configure logger
    set_logging.main(args.infile.split('.fil')[0]+'_inspectra',args.verbose)

    main()
