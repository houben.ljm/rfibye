# RFIbye

This set of python modules is designed to remove RFI from Filterbank files the "brute force" way. This is done by applying thresholds to a given Filterbank file and replacing all data elements that exceed the set thresholds with noise. In order to allow an observer to determine the "appropriatness" of a given threshold, this repository comes with its own plotting tool. Use *inspectra.py* to look at the data and visually inspect what parts will be influenced by *RFIbye.py* with the set thresholds. If one wishes to evaluate the quality of the applied mask at a later time, it is also possible to write the applied mask to file and inspect it later.

Since each module has an intrinsic function, each module will be explained in detail below. *RFIbye.py* is the core script and can be used for the following things:
-  Remove a Filterbank's band edges
-  Remove manually set frequency channels
-  Auto remove bad channels which SNRs exceed a set threshold in the frequency domain
-  Hard apply a PRESTO rfifind mask
-  Remove spectra exceeding a set SNR threshold in the time 
-  Downsample the data in the time domain and remove spectra exceeding a set SNR threshold
-  Remove spectra which standard deviation exceed a set theshold
-  Remove data elements that either:
   1.  Have a value within a given percentage of the highest allowed value of the data's data type (only possible for interger data)
   2.  Have a value exceedig a set SNR threshold in the corresponding channel
-  Remove spectra that regularly repeat with a given (or determined) period (see examples)
-  Apply a zero DM filter
-  Remove the Filterbank's bandpass

Data samples that are flagged as bad by *RFIbye.py* are replaced with random "valid" data samples from the same channel. This is done to preserve the same noise statistics of the channel and truly prevent single pulse search engines to detect false positives. When replacing bad samples with pure Gaussian noise, the replaced samples will otherwise remain visible in the data, having a more noisy background.

## Example
Below is a 0.1 seconds long integer Filterbank chunk shown that contains both broad band RFI spikes and very narrow RFI "speckles". Both these two types of RFI were removed together with the bandpass edges and some manually specified channels zapped. This was all done in the following manner:
```
RFIbye.py --ts_thres 5 --chan_thres 5 --edge 5 --rm_band_edge --zap_chans 111,112,113,333,444-460 example.fil
```
![Inspectra plot of RAW Filterbank file.](/images/RFIbye_withoutmask.png "Figure 1: An inspectra instance, showing the RAW Filterbank of which an RFIbye mask is determined.")\
The `--edge 5` option causes 5 data elements around a classified RFI element to be flagged as well. In the mask (below) this is visible as bigger squares around the RFI "speckles" and ensures that any power bleed to surrounding data elements is also removed.\
Compare the two images to see what *RFIbye.py* has all classified as RFI with the settings presented above.

![Inspectra plot of RAW Filterbank file and overlayed the corresponding RFIbye mask.](/images/RFIbye_withmask.png "Figure 2: An inspectra instance, showing the RAW Filterbank overlayed with its corresponding RFIbye mask.")\
*RFIbye.py* hard applies masks to a copy of the given Filterbank file by replacing masked data elements with random noise equal to the average noise distribtion of the analysed data block. The result of the application of the above mask can be seen here (below).\
Notice, that once a complete channel is flagged it is replaced with noise of a median and rms that are determined by linear interpolation between the neighboring "valid" channels. The effect of this linear interpolation can be seen for the channels just below a frequency of 1250 MHz. Likewise, are the band edges replaced with noise of similar property as the closest "valid" channels.

![Inspectra plot of the masked Filterbank file.](/images/RFIbye_cleaned.png "Figure 3: An inspectra instance, showing the masked Filterbank, i.e. the output of RFIbye.py.")

**There are two things that are good to point out here.**
1. *RFIbye.py* can read PRESTO's rfifind mask. I.e. it can be used to hard apply *rfifind* masks to Filterbank data.
2. If RFI shows as a periodic signal in the time series with a constant period, *RFIbye.py* can remove it! Just set the option `--rm_reg`. You can specify the period if you know it before hand. Or use a pop-up inspectra instance to manually determine the period with two simple mouse clicks. Alternativally, *RFIbye.py* can determine the period of present periodic RFI automatically. Just give a period of 0 and let *RFIbye.py* do the work for you. In the inspectra instance below is a periodic RFI shown overlayed on the mask, automatically determined by *RFIbye.py* using a period of 16 samples.

![Inspectra plot of regular RFI.](/images/RFIbye_wiggly_masked.png "Figure 4: An inspectra instance, showing regular RFI overlayed on a mask, automatically determined by RFIbye.py to remove it.")

## Getting Started
### Prerequisites
To run *RFIbye.py*, one needs the latest verion of [`PRESTO`](https://github.com/scottransom/presto).\
Both codes depend on python 3, so ensure you have it installed together with the non-standard modules: `matplotlib`, `numpy`, `scipy` and `bottleneck`.\
Keep all scripts in this repository together, since they are all interdependent and need each other to work properly.

### Installing
Once you installed the above dependencies, installing the code is as simple as doing a git clone.\
Say bye to RFI!

## How to use the codes in this repository
### RFIbye.py
As mentioned in the introduction, *RFIbye.py* can be used to remove several types of RFI from Filterbank files and replace them with random noise that has a similar noise distribution per channel as the original data. Thus, *RFIbye.py* **HARD APPLIES** any determined mask to (a copy of) your data! A useful feature, since such you can use your favourite analysis tools on a cleaned Filterbank file and so hopefully find the signals of interest. Thus, start using this script and say bye to RFI!

The first time you use *RFIbye.py*, it is recommended to try out its options to see which settings give you the best results. Luckily, this is made easy with the option `--inspec`, which plots your data together with the mask to be applied. As such, you can quickly try several parameter values and see how they affect your data.

If you don't want to check determined masks during run time, you can do this at a later moment with the option `--write_mask`. This will make *RFIbye.py* write its determined mask to file, which then can be inspect with *inspectra.py*.

Below is a slightly more detailed explanation given for each of *RFIbye's* options than found when using the `-h` option.

Note: *RFIbye.py* is, as of yet, an intrinsically slow (slower than realtime) code due to the replacement of RFI by random noise with an equal noise distribution as the data's background noise. The background noise has to be calculated accurately in order to not replace RFI with data having a different noise distribtion and still stick out from the data. So, possibly being detected as RFI by analysis tools again.

**Usage:**
```
RFIbye.py        [-h] [--ts_thres [SNR]] [--ts_downsampfrac [2**pow]] [--std_thres [SNR]]
                 [--chan_thres [SNR]] [--chan_int_extr_thres percent] [--edge EDGE] 
                 [--full_spec_flag percent] [--rm_reg [period]] [--rm_band_edge [percent]]
                 [--rm_bandpass] [--chan_indep] [--zap_chans chan numbers] [--zap_chan_thres [SNR]]  
                 [--rfifind_mask Path to file] [--zerodm] [--block_size BLOCK] 
                 [--write_mask [Mask file name]] [--inspec] [-j [NCORES]] [-o OUTNAME] [-v]
                 infile
```

**Positional arguments:**
```
  infile                Filterbank file that needs some RFI blasted away.
```

**Optional arguments:**
```
  -h, --help            Show help message and exit.

  -n, --norm_data       Identify RFI using normalised data (i.e. on data with its bandpass 
                        removed: (data - med) / std)
  
  --ts_thres [SNR]      Flag entire spectrum whenever its summed values exceed
                        [SNR] times the standard diviation of the time series.
                        (Default: 4.0)

  --ts_downsampfrac [2**pow]
                        Threshold the time series again with the set 'ts_thres'
                        threshold but after it has been downsampled with the given
                        power of 2 in time. (Default: 2**3)

  --std_thres [SNR]     Flag entire spectra whenever a spectra's standard deviation
                        exceeds the set SNR threshold. (Default: 8.0)

  --chan_thres [SNR]    Flag individual time,frequency samples per channel.
                        For integer type data and no set threshold, data elements
                        are flagged that fall within the highst percentage of the
                        allowed dtype values set with --chan_int_extr_thres.
                        Else, those values exceeding the set SNR threshold will
                        be flagged. (Default, for float data: 10.0)

  --chan_int_extr_thres percent
                        When channel thresholding integer data, remove this percentage
                        of the max dtype range instead of using an SNR. (Default: 2.5%)

  --edge EDGE           Set how many samples need to be flagged around a
                        classified RFI sample.

  --full_spec_flag percent
                        Set the percentage of flagged channels above which a
                        spectrum will be fully flagged. (Default, 30)

  --rm_reg [period]     Flag potential regularities in the time series with the
                        given period (in samples).
                        If no period is given, you will be prompted with an
                        inspectra instance in which to indicate the period of
                        the regular RFI.
                        If a period of 0 is given, RFIbye.py will auto search
                        and remove periodic regions with a period between 1 and
                        2**8 samples.

  --rm_band_edge [percent]
                        Remove the given percentage of the top and bottom of
                        the band. (Default: 1.0%)

  --rm_bandpass         Subtract bandpass median and devide by data standard deviation.

  --chan_indep          When subtracting bandpass, devide by standard deviation of
                        each channel instead of entire data BLOCK. (Default: False)

  --zap_chans chan numbers
                        Comma seperated list (no spaces!) of channel numbers
                        to flag, where channel 0 is the highest frequency
                        channel. (See example above!)

  --zap_chan_thres [SNR]
                        Flag a channel whenever its power in the frequency domain
                        exceeds the set SNR. (Default: 6.0)

  --full_chan_zap [percent]
                        Set the percentage of flagged spectra above which a channel
                        will be fully zapped. (Default: 20.0)

  --rfifind_mask Path to file
                        Path to a PRESTO rfifind mask to apply to data.

  --zerodm              Apply zero-DM filter to data to (hopefully) remove
                        broad-band RFI.

  --block-size BLOCK    Number of spectra per read. This is the amount of data
                        processed at a time. This defaults either to 2**15 samples
                        or the 'ptsperint' value in a given rfifind mask.

  --write_mask [Mask file name]
                        Write the flagged samples to an RFIbye mask with the given
                        file name. If no file name is given, the base name of the
                        processed Filterbank file is appended with '.rfibyemask'.

                        Note 1: if '--rfifind_mask' is set, the resulting .rfibyemask
                        can become rather large depending on the set integration time
                        of the rfifind mask!

                        Note 2: an RFIbye mask is hard code limited to write a maximum
                        of 32767 channels to file, this to reduce the resulting file size.

  --inspec              Show GUI to inspect the quality of the applied mask.
                        See the inspectra.py explanation below for details on how
                        to interact with the GUI.

  -j [NCORES], --ncores [NCORES]
                        Number of cores to use for processing. If no number is specified
                        it defaults to the number of available CPUs.

                        Note: if set, no inspectra instances are shown, nor will masks
                        be written to disk in case any of these options are given.
                        Another issue of multi-processing the data is that, in the rare
                        cases in which entire data BLOCKs are masked, such a data BLOCK
                        will be replaced with noise of a different distribution as the data.
                        This will most probably cause the boundaries of this data BLOCK to
                        show as very narrow RFI by analysis tools.

  -o OUTNAME, --outname OUTNAME
                        Name of output Filterbank file.
                        If unset, the name of the input Filterbank file name is 
                        used and appended with '_MASKED.fil'.

  -v, --verbose         Print the operation details to screen as well as to a .log file.
```

### inspectra.py
*Inspectra.py* is a Filterbank plotting tool similar to PRESTO's *waterfaller.py*, with a few exceptions and differences. One can use *inspectra.py* only to view the content of a Filterbank file, it can not do any data manipulation other than extracting the data's bandpass before plotting (option `--rm_bandpass`). However, it can overlay the data with an .rfibyemask, to see which parts of the data are affected by the mask.\
Also, it allows to "loop" through the plotted data. In other words, one does not need to launch inspectra.py over and over again to view different parts of the Filterbank file. You can interact directly with [the prompted GUI](https://gitlab.com/houben.ljm/rfibye#example) to go to different parts of the data. Though, as of now, only forward in time, not backwards.

**How to interact with the GUI launched by *inspectra.py*.**\
Several keyboard keys have been assigned special functions to be able to interact with [the shown GUI](https://gitlab.com/houben.ljm/rfibye/#example). These keys and their function are:
- **'<'** Show *previous* part of the loaded data BLOCK.
- **'>'** Show *next* part of the loaded data BLOCK.
- **'m'** Show *the first* part of the loaded data BLOCK.
- **'/'** Show *the last* part of the loaded data BLOCK.
- **'c'** Show *the center* of the loaded data BLOCK.
- **'['** Toggle mask view *off*.
- **']'** Toggle mask view *on*.
- **'q'** Close GUI and exit.
- **'d'** Close GUI and exit. Like 'q', will result in *RFIbye.py* applying its mask without showing it first from here onward.
- **'Enter'** Close GUI and show next block of data.
- **'n'** Close GUI and show next block of data.

Special interaction keys:
- **'p'** Turn on 'picking', to allow selection of specific data point.
- **'t'** Take the selected data point's values (for internal code usage).

**Usage:**
```
inspectra.py        [-h] [--rfibye_mask Path to file] [--rm_bandpass]
                    [--chan_indep] [-S START] [-T TIME] [-B BLOCK] [-v]
                    infile
```

**Positional arguments:**
```
  infile                Filterbank file of which you want to inspect its spectra.
```

**Optional arguments:**
```
  -h, --help            Show help message and exit.

  --rfibye_mask Path to file
                        Show RFIbye mask to assert its affect on the loaded data.

  --rm_bandpass         Remove data's bandpass before plotting.

  --chan_indep          Devide data by std of each channel (instead of global std)
                        when removing bandpass.

  -S START, --start START
                        Number of seconds into data to start plotting spectra.
                        (Default: 0.0)

  -T TIME, --time TIME  Duration (in seconds) of the extracted data per read.
                        Takes precedence over the 'block-size'.

  -B BLOCK, --block-size BLOCK
                        Number of spectra per read. (Default: 2**13 samples)

  -v, --verbose         Print the operation details to screen as well as to a .log file.
```

### mask_maker.py
Script used to read RFIbye and rfifind mask files as well as to write RFIbye masks.\
Because *inspectra.py* can only overlay RFIbye masks, *mask_maker.py* can nativaly be used to convert PRESTO rfifind masks to RFIbye masks. Depending on the set integration time of the rfifind mask, this conversion can, however, take a long time to complete and result in a large mask file. Once complete, you can then use the RFIbye mask to overlay the content of the rfifind mask on your Filterbank data with *inspectra.py*.

Note: *mask_maker.py* can only write RFIbye masks with a maximum number of **32767 channels**. This is hard coded into the mask type in order to keep the mask files small.

**Usage:**
```
mask_maker.py [-h] rfifind_mask
```

**Positional arguments:**
```
  rfifind_mask  Path to PRESTO rfifind mask to convert to an RFIbye mask.
```

### fil_looper.py
A convenient Filterbank data handler that loops through the Filterbank file, importing BLOCK number of spectra per read and feeding them to a custom function. The output of this custom function can then be written to a new Filterbank file.

Just for fun, is *fil_looper.py* build to nativaly downsample Filterbank files in time.

**Usage:**
```
fil_looper.py        [-h] [--frac FRAC] [--block-size BLOCK] [-o OUTNAME] [-v]
                     infile
```

**Positional arguments:**
```
  infile                Filterbank file to downsample.
```

**Optional arguments:**
```
  -h, --help            Show help message and exit.

  --frac FRAC           Fraction with which to downsample the data in time. (Default: 2)

  --block-size BLOCK    Number of spectra per read. This is the amount of data
                        processed at a time. (Default: 32768)

  -j [NCORES], --ncores [NCORES]
                        Number of cores to use for processing. (default: False)

  -o OUTNAME, --outname OUTNAME
                        Name of output Filterbank file.

  -v, --verbose         Print the operation details to screen as well as to a .log file.
```

## Finally
### Author
*  [**Leon Houben**](https://www.linkedin.com/in/houbenljm) - Radboud University / Max Planck for Radio Astronomy

### Acknowledgements
Many thanks to *Tom Stock* whom provided me with lots of tips and feedback on how to implement common python conventions in this repository, as well as for his patience answering all my questions.
