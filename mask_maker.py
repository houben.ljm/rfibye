#!/usr/bin/env python

"""
mask_maker.py

Opens either a mask or creates one.
Code can read:
    - an rfifind mask; then returns a numpy array with channels to zap for a given integration
    - an rfibye mask; then returns a bolean numpy array which is the exact mask for a given integration / data block.
If no existing mask file is given, the code will create an rfibye mask file.

#NOTE: for the creation of an RFIfind mask, the number of channels of the corresponding Filterbank file is assumed to be smaller than 32767!!!

Leon Houben
"""

import os,sys
import argparse
import logging
import set_logging
import numpy as np

logger = logging.getLogger(__name__)

class masker(object):
    def __init__(self,fn_mask,**data_dict):
        """masker constructor
            Give details here...
        """
        ## Check if mask file exists, otherwise create new rfibye mask
        self.fn_mask = fn_mask

        if os.path.isfile(self.fn_mask):
            # Read rfifind or RFIbye mask
            self.fmask = open(self.fn_mask)
            if self.fn_mask.endswith(".rfibyemask"):
                self.mask_type   = "rfibye"
                self.last_nspec  = 0
                self.last_chan_list = None
                self.block_size  = None
                self.ispec_start = 0
                self.initialise_rfibye_mask()
            elif self.fn_mask.endswith(".mask"):
                self.mask_type = "rfifind"
                self.read_rfifind_mask()
            else:
                log_str = "\nCould not determine if the given file (%s) is an RFIbye mask or rfifind mask..." % self.fn_mask
                logger.error(log_str)
                raise NotImplementedError(log_str)

        else:
            # Write RFIbye mask
            self.fmask = open(self.fn_mask+'.rfibyemask', mode='w+')
            self.write_to_mask = False
            self.spec_index = 0

            if not data_dict:
                log_str = "To create a new RFIbye mask, the following Filterbank file details should be given:\n \
                        - MJD\n- dtint\n- fch1\n- df\n- nchan\n- nint"
                logger.error(log_str)
                raise ValueError(log_str)
            else:
                self.MJD       = data_dict['MJD']
                self.dtint     = data_dict['dtint'] #NOTE: this is the sampling time of the corresponding data file
                self.fch1      = data_dict['fch1']
                self.df        = data_dict['df']
                self.nchan     = data_dict['nchan']
                self.nint      = data_dict['nint'] #NOTE: this is the number of spectra in the corresponding data file
                self.ptsperint = 1 

    def make_rfibye_mask(self,chan_list):
        """
            Input:
                chan_list:  a list or numpy array of complete channels to zap
        """
        ## First write Filterbank file specifics to mask file
        np.array([self.MJD,self.dtint,self.fch1,self.df]).astype(np.float64).tofile(self.fmask)
        np.array([self.nchan,self.nint,self.ptsperint]).astype(np.int32).tofile(self.fmask)

        ## Second write the complete channels to be zapped to mask file
        self.zap_chans    = np.array(chan_list, dtype=int)
        self.n_good_chans = self.nchan - self.zap_chans.shape[0]
        zap_channels      = np.append(self.zap_chans.shape[0],self.zap_chans).astype(np.int16)
        zap_channels.tofile(self.fmask)

        ## Third enable writing to mask file
        self.write_to_mask = True

        logger.info("    RFIbye mask created and ready to write mask info to disk.")
        return

    def write_to_rfibye_mask(self,bool_mask):
        """
            Input:
                bool_mask:  a boolean numpy array, where True values denote flagged samples.
                            Bool_mask is assumed to be of shape (nchan,nint).

            Output:
                Mask file with flagged samples appended to it.
        """
        if not self.write_to_mask:
            log_str = "\nAn RFIbye mask must be created first before one can write to it."
            logger.error(log_str)
            raise IOError(log_str)

        # Remove fully flagged channels from mask
        bool_mask[self.zap_chans] = False

        # Determine wich data elements were flagged
        zap_spec_chans = np.argwhere(bool_mask.T)
        ispecs,counts  = np.unique(zap_spec_chans[:,0], return_counts=True)

        # Loop over ispecs and append channels to be flagged per spectrum
        islice = 0
        for idxspec,ispec in enumerate(ispecs):

            if counts[idxspec] == self.n_good_chans:
                flag_info = np.array([self.spec_index+ispec,self.nchan]).astype(np.int32)
                flag_info.tofile(self.fmask)
            else:
                flag_info = np.array([self.spec_index+ispec,counts[idxspec]]).astype(np.int32)
                flag_info.tofile(self.fmask)
                zap_spec_chans[islice:islice+counts[idxspec],1].astype(np.int16).tofile(self.fmask)

            islice += counts[idxspec]

        # Keep track on how many spectra have been processed
        self.spec_index += bool_mask.shape[1]

        # Close mask file if entire Filterbank file has been processed
        if self.spec_index >= (self.nint):
            logger.info("\nRFIbye mask closed.")
            self.fmask.close()

        logger.info("        DONE: %i spectra processed. Mask info for %i spectra written to file." \
                    % (bool_mask.shape[1],len(ispecs)))
        return

    def mask(self,Nblock):
        if self.mask_type == "rfifind":
            return self._mask[int(Nblock)]
        else:
            if self.block_size is None:
                log_str = "The block size needs to be set before masks can be extracted from file!"
                logger.error(log_str)
                raise ValueError(log_str)

            start_nspec = max(int(self.ispec_start + Nblock * self.block_size - 1), 0)
            if (start_nspec+self.block_size) < self.last_nspec:
                # Reload mask to enable re-read of a previous mask block
                self.fmask.close()
                self.fmask = open(self.fn_mask)
                self.last_nspec  = 0
                self.last_chan_list = None
                self.initialise_rfibye_mask()
            self.read_rfibye_mask(start_nspec)
            return self._mask

    def initialise_rfibye_mask(self):
        """
        Read the filterbank file properties from an rfibye mask.
        """
        self.MJD, self.dtint, self.fch1, self.df = \
                       np.fromfile(self.fmask, dtype=np.float64, count=4)
        self.nchan, self.nint, self.ptsperint = np.fromfile(self.fmask, dtype=np.int32, count=3)
        nzap = np.fromfile(self.fmask, dtype=np.int16, count=1)[0]
        if nzap:
            self.zap_chans = np.fromfile(self.fmask, dtype=np.int16, count=nzap)
        else:
            self.zap_chans = np.asarray([], dtype=int)

        if len(self.zap_chans)==self.nchan:
            logger.warning("WARNING!:  All channels recommended for masking!")

        logger.info("    RFIbye mask initialised...")
        return

    def read_rfibye_mask(self,start_nspec):
        """
        Read which channels to zap from a single spectrum from an rfibye mask.
        """
        end_nspec = min(start_nspec+self.block_size,self.nint)
        mask = np.zeros([self.nchan,end_nspec-start_nspec]).astype(bool)
        # Flag all channels
        mask[self.zap_chans] = True
        maskT = mask.T

        if (self.last_nspec > start_nspec) and (self.last_nspec < end_nspec):
            zap_spec_chans = np.union1d(self.last_chan_list,self.zap_chans) # Add completly zapped channels
            maskT[self.last_nspec % start_nspec,zap_spec_chans] = True

        logger.info("        Reading next block of RFIbye mask data...")
        while True:
            try:
                ispec, nzap = np.fromfile(self.fmask, dtype=np.int32, count=2)
            except ValueError:
                logger.exception("\nReached end of mask file! Closing mask file...")
                self.fmask.close()
                break
            else:
                if nzap == self.nchan:
                    zap_spec_chans = np.arange(0, self.nchan, dtype=np.int16)
                else:
                    zap_spec_chans = np.fromfile(self.fmask, dtype=np.int16, count=nzap)

                if ispec < start_nspec:
                    continue
                elif (ispec >= start_nspec) and (ispec < end_nspec):
                    zap_spec_chans = np.union1d(zap_spec_chans,self.zap_chans) # Add completly zapped channels
                    if start_nspec == 0:
                        maskT[ispec,zap_spec_chans] = True
                    else:
                        maskT[ispec % start_nspec,zap_spec_chans] = True
                else:
                    self.last_nspec = ispec
                    self.last_chan_list = zap_spec_chans
                    break

        self._mask = maskT.T

        logger.info("        DONE: mask info from spec num %i to %i recovered from file." % (start_nspec,end_nspec))
        return

    def read_rfifind_mask(self):
        """
        Adapted version of 'read_mask' from PRESTO's 'rfifind.py'.
        The function is copied here so only the mask information is 
        loaded to increase the processing speed. To save memory are
        only the useful parameters stored from the rfifind mask.
        """
        time_sig, freq_sig, self.MJD, self.dtint, self.fch1, self.df = \
                       np.fromfile(self.fmask, dtype=np.float64, count=6)
        self.nchan, self.nint, self.ptsperint = np.fromfile(self.fmask, dtype=np.int32, count=3)
        nzap = np.fromfile(self.fmask, dtype=np.int32, count=1)[0]
        if nzap:
            self.zap_chans = np.fromfile(self.fmask, dtype=np.int32, count=nzap)
        else:
            self.zap_chans = np.asarray([])
        if len(self.zap_chans)==self.nchan:
            logger.warning("    WARNING!:  All channels recommended for masking!")
        nzap = np.fromfile(self.fmask, dtype=np.int32, count=1)[0]
        if nzap:
            self.zap_ints = np.fromfile(self.fmask, dtype=np.int32, count=nzap)
        else:
            self.zap_ints = np.asarray([])
        if len(self.zap_ints)==self.nint:
            logger.warning("    WARNING!:  All intervals recommended for masking!")
        nzap_per_int = np.fromfile(self.fmask, dtype=np.int32, count=self.nint)
        self.zap_chans_per_int = []
        for nzap in nzap_per_int:
            if nzap:
                if nzap == self.nchan:
                    tozap = np.arange(0, self.nchan, dtype=np.int32)
                else:
                    tozap = np.fromfile(self.fmask, dtype=np.int32, count=nzap)
            else:
                tozap = np.asarray([])
            self.zap_chans_per_int.append(tozap)
        self.fmask.close()

        if self.df > 0:
            # Reverse freq order of channels to zap
            self.zap_chans = self.nchan-1-self.zap_chans
            # Reverse freq order of rfifind mask
            reversed_mask  = []
            for integration in self.zap_chans_per_int:
                reversed_mask.append(self.nchan-1-integration)
            self._mask = reversed_mask
            self.fch1  = (self.nchan-1)*self.df + self.fch1
            self.df   *= -1

        logger.info("        DONE: rfifind mask succesfully loaded.")

    @property
    def freqs(self):
        return np.arange(self.nchan)*self.df + self.fch1

    @property
    def times(self):
        return np.arange(self.nint)*self.dtint

    @property
    def MJDs(self):
        return self.times/86400.0 + self.MJD

def main():
    # Initialise rfifind mask
    rfifind_masker = masker(args.rfifind_mask)

    # Initialise rfibye mask
    mask_details = {'MJD'   : rfifind_masker.MJD, \
                    'dtint' : (rfifind_masker.dtint/rfifind_masker.ptsperint), \
                    'fch1'  : rfifind_masker.fch1, \
                    'df'    : rfifind_masker.df, \
                    'nchan' : rfifind_masker.nchan, \
                    'nint'  : (rfifind_masker.nint*rfifind_masker.ptsperint)}
    rfibye_masker = masker(args.rfifind_mask.split('.mask')[0]+'_rfifind',**mask_details)

    # Convert rfifind mask to rfibye mask
    logger.info("\nConverting %s to an RFIbye mask..." % args.rfifind_mask)
    logger.warning("    This might take long, depending on the set integration time of the rfifind mask.")
    oldprogress = 0
    sys.stdout.write(" %3.0f %%\r" % oldprogress)
    sys.stdout.flush()

    rfibye_masker.make_rfibye_mask(rfifind_masker.zap_chans)
    for Nblock in np.arange(rfifind_masker.nint):
        mask = np.zeros([rfifind_masker.nchan,rfifind_masker.ptsperint]).astype(bool)
        mask[rfifind_masker.mask(Nblock)] = True
        rfibye_masker.write_to_rfibye_mask(mask)

        progress = int(100.0*Nblock/rfifind_masker.nint)
        if progress > oldprogress:
            sys.stdout.write(" %3.0f %%\r" % progress)
            sys.stdout.flush()
            oldprogress = progress

    rfifind_masker.fmask.close()
    sys.stdout.write("Done\n\n")
    sys.stdout.flush()



if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='mask_maker.py', formatter_class=argparse.ArgumentDefaultsHelpFormatter, \
                                     description="Convert an rfifind mask to an RFIbye mask.")

    parser.add_argument("rfifind_mask", \
                            help="PRESTO's rfifind mask to process.")

    args = parser.parse_args()
    if not os.path.isfile(args.rfifind_mask):
        log_str =  "\n%s does not exist!" % args.rfifind_mask
        logger.error(log_str)
        raise IOError(log_str)

    #Configure logger
    set_logging.main(args.rfifind_mask.split('.mask')[0],True)

    main()
